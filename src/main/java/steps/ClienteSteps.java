package steps;

import common.BasePO;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import pageobjects.ClientePage;
import pageobjects.LoginPage;
import pageobjects.MenusPage;

public class ClienteSteps {

	@Dado("^abro na pagina de login e insiro meu usuario e senha$")
	public void loginNoSistema() {
		LoginPage loginPage = new LoginPage();
		BasePO basePO = new BasePO();
		loginPage.efetuarLoginNaPagina();
		basePO.waitForLoading();
		loginPage.aguardarBotaoLoginDesaparecer();
		//loginPage.cupom(); -> usar em outro momento
	}

	@E("^acessei o menu Perfil$")
	public void acessoMenuPerfil(){
		MenusPage menusPage = new MenusPage();
		menusPage.acessarMenusPerfil();
	}

	@E("^acessei o menu Cursos$")
	public void acessaMenuCursos() {
		MenusPage menusPage = new MenusPage();
		menusPage.acessarMenuCursos();
	}

	@E("^na aba Cursos e Trilhas$")
	public void cursosTrihlas() {
		MenusPage menuPage = new MenusPage();
		menuPage.cursosTrilhas();
	}

	@E("^na aba Cursos Meu micro-momento$")
	public void meuMicroMomento() {
		MenusPage menuPage = new MenusPage();
		menuPage.microMomento();
	}

	@E("^na aba Cursos Todos Cursos e Trilhas$")
	public void todoCursoTrilhas() {
		MenusPage menuPage = new MenusPage();
		menuPage.todosCursosTrilhas();
	}

	@E("^na aba Cursos Biblioteca$")
	public void subMenuBiblioteca() {
		MenusPage menuPage = new MenusPage();
		menuPage.menuBiblioteca();
	}
	/* Area de Ajuda */
	@E("^acessei o menu Ajuda$")
	public void acessaMenuAjuda() {
		MenusPage menusPage = new MenusPage();
		menusPage.menuAjuda();
	}

	@E("^na aba Sobre o Lit$")
	public void abaSobreLit() {
		MenusPage menuPage = new MenusPage();
		menuPage.subMenuSobreLit();
	}

	@E("^na aba Help Desk$")
	public void helpDesk() {
		MenusPage menuPage = new MenusPage();
		menuPage.subMenuHelp();
	}

	@Entao ("^na tela Dados de Identificacao informo os dados de Pessoa Fisica$")
	public void informoCurso() {
		ClientePage clientePage = new ClientePage();
		clientePage.clicandoNoMenuCliente();
		clientePage.acessadoMenuCliente();
	}

	@Dado("que clico no login por facebook")
	public void logarFace() {
		LoginPage loginPage = new LoginPage();
		loginPage.loginFacebook();
	}

	@E("coloco o email")
	public void informarEmail() {
		LoginPage loginPage = new LoginPage();
	}

	@E("senha")
	public void senhaFace() {
		LoginPage loginPage = new LoginPage();
	}

	@Entao("clico em acessar")
	public void acessaFacebook() {
		LoginPage loginPage = new LoginPage();
	}

	@Dado("que clico no login por google")
	public void logarGoogle() {
		LoginPage loginPage = new LoginPage();
		loginPage.loginGoogle();
	}

	@E("coloco o email google")
	public void informarEmailGoogle() {
		LoginPage loginPage = new LoginPage();
	}

	@E("senha google")
	public void senhaGoogle() {
		LoginPage loginPage = new LoginPage();
	}

	@Entao("clico em acessar google")
	public void acessaGoogle() {
		LoginPage loginPage = new LoginPage();
	}

	@Dado("que clico no login por linkedin")
	public void logarLinkedin() {
		LoginPage loginPage = new LoginPage();
		loginPage.loginLinkedin();

	}

	@E("coloco o email linkedin")
	public void informarEmailLinkedin() {
		LoginPage loginPage = new LoginPage();
	}

	@E("senha linkedin")
	public void senhaLinkedin() {
		LoginPage loginPage = new LoginPage();
	}

	@Entao("clico em acessar linkedin")
	public void acessaLinkedin() {
		LoginPage loginPage = new LoginPage();
	}
} 