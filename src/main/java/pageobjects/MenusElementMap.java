package pageobjects;

import common.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MenusElementMap {

	public MenusElementMap() {
		PageFactory.initElements(BaseTest.getDriver(), this);
	}

	@FindBy(xpath = "//div[@class='col-10 objetivos-personalizar--desc-usuario text-body-pi']")
	protected WebElement mensagemInicial;

	@FindBy(xpath = "//button[@class='btn btn-blue btn-FontWeight6']")
	protected WebElement btnComecar;

	@FindBy(xpath = "//img[@class='rounded-circle']")
	protected WebElement menuCliente;

	@FindBy(xpath = "//div[@class='dropdown-menu dropdown-menu-right show']/a[1]")
	protected WebElement btnPerfil;

	/* Menu Cursos */

	@FindBy(xpath = "//span[text()='Cursos']")
	protected WebElement btnMenuCursos;

	@FindBy(xpath = "//div[@class='dropdown-menu dropdown-menu-right show']/a[1]")
	protected WebElement btnCursosTrilhas;

	@FindBy(xpath = "//div[@class='dropdown-menu dropdown-menu-right show']/a[2]")
	protected WebElement btnMicroMomento;

	@FindBy(xpath = "//div[@class='dropdown-menu dropdown-menu-right show']/a[3]")
	protected WebElement btnTodoCursosTrilhas;

	@FindBy(xpath = "//div[@class='dropdown-menu dropdown-menu-right show']/a[4]")
	protected WebElement btnBiblioteca;

	/* Menu Social */

	/* Menu Ajuda */

	@FindBy(xpath = "//span[text()='Ajuda']")
	protected WebElement menuAjuda;

	@FindBy(xpath = "//div[@aria-labelledby='navbarAjuda']/a[1]")
	protected WebElement btnSobreLit;

	@FindBy(xpath = "//div[@aria-labelledby='navbarAjuda']/a[2]")
	protected WebElement btnHelpDesk;
}
