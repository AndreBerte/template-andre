package pageobjects;

import common.BaseTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ClientePage extends ClienteElementMap {

	private WebDriver driver;
	private WebDriverWait wait;

	public ClientePage() {
		driver = BaseTest.getDriver();
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(BaseTest.TIMEOUT));
	}

	public void clicandoNoMenuCliente() {
		// TODO Auto-generated method stub

	}

	public void acessadoMenuCliente() {
		// TODO Auto-generated method stub

	}
	public void clicarAvancar() {
		avancar.click();

	}

}
