package pageobjects;

import common.BasePO;
import common.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static common.ListenerTest.reportPrintInfo;

public class MenusPage extends MenusElementMap {

	private WebDriver driver;
	private WebDriverWait wait;
	private BasePO basePO;

	public MenusPage() {
		driver = BaseTest.getDriver();
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(BaseTest.TIMEOUT));
		basePO = new BasePO();
	}

	public void acessarMenusPerfil(){
		basePO.waitForLoading();
		try {
			wait.until(ExpectedConditions.visibilityOf(mensagemInicial));
			wait.until(ExpectedConditions.elementToBeClickable(btnComecar));
			btnComecar.click();
		}catch (Exception e){
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(basePO.getLocator(menuCliente))));
			menuCliente.click();
			wait.until(ExpectedConditions.elementToBeClickable(btnPerfil));
			btnPerfil.click();
		}
	}

	public void acessarMenuCursos() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnMenuCursos));
		btnMenuCursos.click();
	}

	public void telaInicial() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(menuCliente));
		menuCliente.click();
	}

	/* Area de Menu para Cursos */
	public void cursosTrilhas() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnCursosTrilhas));
		btnCursosTrilhas.click();
	}

	public void microMomento() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnMicroMomento));
		btnMicroMomento.click();
		reportPrintInfo();
	}

	public void todosCursosTrilhas() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnTodoCursosTrilhas));
		btnTodoCursosTrilhas.click();
	}

	public void menuBiblioteca() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnBiblioteca));
		btnBiblioteca.click();
	}

	/* Area de Menus para Ajuda */

	public void menuAjuda() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(menuAjuda));
		menuAjuda.click();
	}

	public void subMenuSobreLit() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnSobreLit));
		btnSobreLit.click();
	}

	public void subMenuHelp() {
		basePO.waitForLoading();
		wait.until(ExpectedConditions.elementToBeClickable(btnHelpDesk));
		btnHelpDesk.click();
	}
}
