package driver;

import environment.CurrentEnvironmentLoader;
import environment.PropertyLoader;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;

import java.net.MalformedURLException;
import java.net.URL;

public enum DriverFactory implements IDriverType {

    FIREFOX {
        public MutableCapabilities returnDriver() {
            return new FirefoxOptions();
        }
    },

    FIREFOX_HEADLESS {
        public MutableCapabilities returnDriver() {
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setHeadless(true);
            return firefoxOptions;
        }
    },

    CHROME {
        @Override
        public MutableCapabilities returnDriver() {
            return defaultChromeOptions();
        }
    },

    CHROME_HEADLESS {
        @Override
        public MutableCapabilities returnDriver() {
            return ((ChromeOptions) defaultChromeOptions())
                    .addArguments("headless", "window-size=1920,1080", "--disable-gpu")
            ;
        }
    },

    SAFARI {
        @Override
        public MutableCapabilities returnDriver() {
            return new SafariOptions();
        }
    },

    EDGE {
        @Override
        public MutableCapabilities returnDriver() {
            return new EdgeOptions();
        }
    };


    private static MutableCapabilities defaultChromeOptions() {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

        ChromeOptions capabilities = new ChromeOptions();
        capabilities.addArguments(
                "--verbose",
                "--disable-web-security",
                "--ignore-certificate-errors",
                "--allow-running-insecure-content",
                "--allow-insecure-localhost",
                "--no-sandbox"
        );
        capabilities.addArguments("start-maximized");
        capabilities.addArguments("lang=pt-BR");
        capabilities.merge(cap);

        return capabilities;
    }


    public static WebDriver criarInstancia(String browser) throws MalformedURLException {
        WebDriver driver;

        switch (PropertyLoader.get().getExecution(CurrentEnvironmentLoader.get())) {

            case "local":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;

            case "local-headless":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver((ChromeOptions) retornaCapacidade("CHROME_HEADLESS"));
                break;

            case "zalenium":
                String zaleniumURL = PropertyLoader.get().getZaleniumUrl();
                RemoteWebDriver remoteWebDriver = new RemoteWebDriver(new URL(zaleniumURL), retornaCapacidade(browser));

                driver = remoteWebDriver;
                break;

            default:
                throw new IllegalArgumentException("Browser no encontrado: " + browser);
        }

        return driver;
    }

    private static MutableCapabilities retornaCapacidade(String browser) {
        return valueOf(browser.toUpperCase()).returnDriver();
    }

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
