package testcases.tests;

import common.BaseTest;
import cucumber.api.CucumberOptions;


@CucumberOptions(
		features = {"classpath:features"},
		plugin = {"json:target/cucumber/report.json", "de.monochromata.cucumber.report.PrettyReports:target/cucumber/relatorio"},
		glue = { "steps", "pageobjects", "testcases/tests", "common" },tags = {"@smoke"},monochrome = true)
public class SmokeTest extends BaseTest {

}
