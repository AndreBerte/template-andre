# language: pt

# Author: Andre Berte
# Sample Template Da Feature

Funcionalidade: Fazer os testes de login nas plataformas

	
  Cenario: login e Perfil
    Dado abro na pagina de login e insiro meu usuario e senha
    E acessei o menu Perfil
  
  
  Cenario: Login e Menu Micro-Momento 
  	Dado abro na pagina de login e insiro meu usuario e senha  
    E acessei o menu Cursos
    E na aba Cursos Meu micro-momento
  
  
  Cenario: Login e Menu Todos Cursos e Trilhas 
  	Dado abro na pagina de login e insiro meu usuario e senha  
    E acessei o menu Cursos
    E na aba Cursos Todos Cursos e Trilhas
	
	
  Cenario: Login e Menu Biblioteca 
  	Dado abro na pagina de login e insiro meu usuario e senha  
    E acessei o menu Cursos
    E na aba Cursos Biblioteca

	
  Cenario: Login e Menu Ajuda
  	Dado abro na pagina de login e insiro meu usuario e senha  
    E acessei o menu Ajuda
    E na aba Sobre o Lit
	
	
  Cenario: Login e Menu Ajuda
  	Dado abro na pagina de login e insiro meu usuario e senha  
    E acessei o menu Ajuda
    E na aba Help Desk
	
	            
  Cenario: Login com Facebook
  	Dado que clico no login por facebook
  	E coloco o email
  	E senha
  	Entao clico em acessar

		
  Cenario: Login com Google
  	Dado que clico no login por google
  	E coloco o email google
  	E senha google
  	Entao clico em acessar google
	
	
  Cenario: Login com Linkedin
  	Dado que clico no login por linkedin
  	E coloco o email linkedin
  	E senha linkedin
  	Entao clico em acessar linkedin
  	     
#    Exemplos: 
#  	| usuario  										  | senha 		|
#	  | andre.fernando.berte@gail.com | AndreBerte|
    
    
    
    
#    When na tela Dados de Identificacao informo os dados de Pessoa Fisica
#    Then na tela Enderecos sera exibida mensagem de sucesso

#  @test
#  Scenario Outline: Title of your scenario outline
#    Given I want to write a step with <name>
#    When I check for the <value> in step
#    Then I verify the <status> in step

#    Examples: 
#      | name  | value | status  |
#      | name1 |     5 | success |
#      | name2 |     7 | Fail    |
