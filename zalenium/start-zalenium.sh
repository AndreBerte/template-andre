#!/bin/bash

docker pull elgalu/selenium

docker run --rm -d --name zalenium -p 4444:4444 \
-e ZALENIUM_EXTRA_JVM_PARAMS="-Dwebdriver.http.factory=apache" \
-v /var/run/docker.sock:/var/run/docker.sock \
--privileged dosel/zalenium start --timeZone "America/Sao_Paulo" \
--screenWidth 1280 --screenHeight 800 \
--desiredContainers 2 --maxDockerSeleniumContainers 10 --maxTestSessions 1 \
--videoRecordingEnabled false

#Garantir Grid Online
contador=0
sleep 5
until curl -sSL http://localhost:4444/wd/hub/status | jq '.value.ready' | grep true; do
  if [ $contador = 5 ]; then
    echo "Esgotado numero de tentativas!"
    break
  fi
  echo "Aguardando Grid..."
  let contador=contador+1
  sleep 10
done
