# Template Automação

Projeto para testes funcionais usando cucumber, JUnit, selenium e testes multiBrowser

 - Testes funcionais web usando o Cucumber + Junit + Selenium

 Passos para buildar:

 - Baixar o código.
 - Buildar o projeto para baixar as dependendências.

 Passos para rodar:

 - Para rodar um teste específico, vá até o arquivo .feature desejado e insira a tag @test no cenário desejado.
 - Após inserir a tag @test, ir na classe (exemplo LoginTest.java), clicar com o botão direito e selecionar a      opção "Run As" -> JUnit Test.

 
 Caso não reconheça alguma linha e essa linha ficar grifada:
 
 - Fechar o arquivo .feature e abrir novamente.
 
#Execucao
Pelo nome da classe:
```maven

mvn clean test -Dtest=SmokeTest
```

Pelo nome da suite:
```maven

mvn clean test -PsuiteWeb -DtestSuite=SmokeTestsSuite
```

Todos os testes:
```maven

mvn clean test
```
